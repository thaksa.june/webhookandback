const { Sequelize } = require('sequelize');
require('dotenv').config()
const {
    DB_name_test,
    DB_username_test,
    DB_password_test,
    DB_host_test,
    DB_name,
    DB_username,
    DB_password,
    DB_host,
    DB_dialect,
    pool_max,
    pool_min,
    pool_acquire,
    pool_idle
} = process.env

let production = false

// * อันนี้เป็นส่วนที่ใช้ในการบอก Sequelize ว่าเราจะ connect ไปที่ไหน
const sequelize = new Sequelize(
    production ? DB_name : DB_name_test, //  ? นี่เป็นชื่อ DB ของเรานะครับ
    production ? DB_username : DB_username_test, // ? user ที่ใช้สรการเข้าไปยัง db
    production ? DB_password : DB_password_test, // ? password 
    {
        host: production ? DB_host : DB_host_test, // ? host ของ db ที่เราสร้างเอาไว้
        dialect: DB_dialect, // ? 'mysql' | 'mariadb' | 'postgres' | 'mssql' พวกนี้ใช่ก็ใช้ได้
        operatorsAliases: false,
        pool: {
            max: parseInt(pool_max),
            min: parseInt(pool_min),
            acquire: pool_acquire,
            idle: pool_idle
        }
    });

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import model


module.exports = db;
