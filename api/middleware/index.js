const jwt = require('jsonwebtoken');

module.exports = {
    ////////////////////////////////////////////////////////////////
    Authentication: async (req, res, next) => {
        try {
        let SplitToken = req.headers.authorization.split(' ');
        const token = SplitToken[1];

        if (!token) {
            return res.status(403).send({ message: "A token is required for authentication" });
        }
            const decoded = jwt.verify(token, TOKEN_SECRET);
            req.user = decoded;
        } catch (err) {
            return res.status(401).send({ message: "Invalid Token" });
        }
        return next();
    }
    ////////////////////////////////////////////////////////////////
}