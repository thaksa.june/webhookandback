const router = require('express').Router()
const db = require('../database/db.configs')
const { Op } = require("sequelize");
const middlware = require('../middleware')


router.post("/search", async (req, res) => {
    let { keyword, limit } = req.body;
    try {
        let findProduct = await db.Product.findAll({
            where: {
                [Op.or]:
                    { name: { [Op.like]: `%${keyword}%` } }
            }, limit
        })
        res.json({ data: findProduct })
    } catch (err) {
        res.json(err)
    }
})

module.exports = router