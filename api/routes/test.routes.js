const router = require('express').Router()
const db = require('../database/db.configs')
const middlware = require('../middleware')

router.get('/test1', (req, res) => {
    res.json('hello')
})

router.get('/auth', middlware.Authentication, (req, res) => {
    res.json('hello')
})


module.exports = router