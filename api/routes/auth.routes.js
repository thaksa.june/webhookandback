const router = require('express').Router()
const db = require('../database/db.configs')
const { Op } = require("sequelize");
const middlware = require('../middleware')

let initState = []

const BscgLineUser = db.BscgLineUser

router.post("/register", (req, res) => {
    let {
        firstname_th,
        lastname_th,
        firstname_en,
        lastname_en,
        position,
        major,
        customer_no,
        contact_phone
    } = req.body;
    console.log('register user')
    let data = { id: initState.length + 1, firstname_th, lastname_th, firstname_en, lastname_en, position, major, customer_no, contact_phone }
    initState.push(data)
    res.status(200).json({
        message: 'register complete',
        status: 200,
        user: data
    })
})

router.get("/:id", (req, res) => {
    console.log(req.params.id)
    let data = initState.filter(item => item.id == req.params.id);
    console.log(data)
    res.status(200).json({
        status: 200,
        user: data
    })
})


router.post("/line_user_create", async (req, res) => {
    let { token, user_id, line_id } = req.body;
    let findUser = await BscgLineUser.findAll({
        // where: { customer_id: user_id, line_id: line_id }
    })
    if (findUser.length < 1) {
        let createUser = await BscgLineUser.create({ line_id, token, customer_id: user_id })
        return res.json({ message: "user created!", user: createUser })
    } else {
        res.json({ message: "line user already!!" })
    }
})

router.get("/line_user/:id", async (req, res) => {
    let id  = req.params.id
    let findUser = await BscgLineUser.findOne({ where: { line_id: id } })
    if (findUser.length < 1) {
        return res.json({ message: "user not found" })
    } else {
        return res.json({ message: "user found", data: { user_id : findUser.customer_id , token : findUser.token } })
    }
})

module.exports = router