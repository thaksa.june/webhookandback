const router = require('express').Router()
const db = require('../database/db.configs')
const { Op } = require("sequelize");
const middlware = require('../middleware')


router.post('/', async (req, res, next) => {
    try {
        let { name } = req.body;
        let findUser = await db.users.findAll({ where: { name: { [Op.like]: `%${name}%` }, shop_id: { [Op.or]: [15, 128] } } })
        res.json({ user: findUser })
    } catch (err) {
        res.json(err)
    }
})

module.exports = router