const express = require('express')
const app = express()
const cors = require('cors')

const webhookController = require('./webhook/webhookController')
const apiController = require('./api/apiControllers')
require('dotenv').config()


app.use(cors({ origin: true }))
app.post(cors({ origin: true }))
app.get(cors({ origin: true }))
app.use(
    express.urlencoded({
        extended: true,
    })
)
app.use(express.json())
app.set('port', process.env.PORT || 3000)

// Test Connection
app.get('/', (_, res) => res.send('bscg line bot hook'))

// **** --> webhooks
app.use(`/${process.env.PROJECT_NAME}/webhooks`, webhookController.webhook)

// TODO --> api
// app.use(`/${process.env.PROJECT_NAME}/api`, apiController)

app.listen(app.get('port'), function () {
    console.log('run at port', app.get('port'))
})
