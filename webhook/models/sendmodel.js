const client = require('../lineCilent.configs')

module.exports = {
    ////////////////////////////////////////////////////////////////
    pushFlexMessage: async (title, flexmessage, who) => {
        let Carousel = {
            type: 'carousel',
            contents: []
        }

        // check flexmessage type
        let type = typeof flexmessage;
        if (type.toUpperCase() === 'OBJECT') {
            Carousel.contents.push(flexmessage)
        } else if (type.toUpperCase() === 'ARRAY') {
            for (let i in flexmessage) {
                message.contents.push(flexmessage[i])
            }
        } else {
            return;
        }

        let message = {
            type: 'flex',
            altText: title,
            contents: Carousel
        }
        return client.pushMessage(who, message);
    },
    ////////////////////////////////////////////////////////////////
    replyFlexMessage: async (title, flexmessage, replyToken) => {
        let Carousel = {
            type: 'carousel',
            contents: []
        }

        // check flexmessage type
        let type = typeof flexmessage;
        if (type.toUpperCase() === 'OBJECT') {
            Carousel.contents.push(flexmessage)
        } else if (type.toUpperCase() === 'ARRAY') {
            for (let i in flexmessage) {
                message.contents.push(flexmessage[i])
            }
        } else {
            return;
        }

        let message = {
            type: 'flex',
            altText: title,
            contents: Carousel
        }
        return client.replyMessage(replyToken, message)
    },
    pushMessage: async (message, who) => {
        const payload = {
            type: 'text',
            text: message
        }
        return client.pushMessage(who, payload)
    },
    ////////////////////////////////////////////////////////////////
    ReplyMessage:async (message, replyToken) => {
        const payload = {
            type: 'text',
            text: message
        }
        return client.replyMessage(replyToken,payload)
    },
    ////////////////////////////////////////////////////////////////
    replyPayload: async (payload, replyToken) => {
        return client.replyMessage(replyToken,payload)
    },
    ////////////////////////////////////////////////////////////////
    pushPayload: async (payload, who) => {
        return client.pushMessage(who, payload)
    },
    ////////////////////////////////////////////////////////////////
}