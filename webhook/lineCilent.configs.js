const line = require('@line/bot-sdk');
require('dotenv').config()

const config = {
    channelAccessToken : process.env.LINE_channelAccessToken,
    channelSecret : process.env.LINE_channelSecret 
}

module.exports =new line.Client(config);