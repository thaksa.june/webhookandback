const db = require('../api/database/db.configs')
const { Op } = require("sequelize");
const flexmodels = require('./models/flexmodel');
const sendmodels = require('./models/sendmodel');


exports.webhook = async function (req, res, next) {
    res.send('HTTP POST request sent to the webhook URL!')
    const event = req.body.events[0]
    const replyToken = event.replyToken;
    const user_id = event.source.userId


    switch (event.type) {
        case 'message':
            break;
        case 'unsend':
            console.log('unsend', event)
            break
        case 'follow':
            console.log('follow', event)
            break
        case 'unfollow':
            console.log('unfollow', event)
            res.sendStatus(200)
            break
        case 'join':
            console.log('join', event)
            break
        case 'leave':
            console.log('leave', event)
            break
        case 'memberJoined':
            console.log('memberJoined', event)
            break
        case 'memberLeft':
            console.log('memberLeft', event)
            break
        case 'postback':
            res.sendStatus(200)
            break
        case 'videoPlayComplete':
            console.log('videoPlayComplete', event)
            break
        case 'beacon':
            console.log('beacon', event)
            break
        case 'accountLink':
            console.log('accountLink', event)
            break
        case 'things':
            console.log('things', event)
            break
        default:
            console.log('event', event)
            res.sendStatus(200)
    }

}